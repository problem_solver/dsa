#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,query,start,end,k;
    cin>>n>>query;
    int arr[n],temp[n]={0};
    for(int i=0;i<n;i++)
        cin>>arr[i];


    for(int i=0;i<query;i++)
    {
        cin>>start>>end>>k;

        //start -> adding by
        if(start>=0 && start<n)
           temp[start]+= k;

        //end+1 ->subtracting -k
        if(end+1>=0 && end+1<n)
            temp[end+1]-=k;
    }
    //prefix sum
    for(int i=1;i<n;i++)
        temp[i]+=temp[i-1];

    // arr+temp
    for(int i=0;i<n;i++)
        cout<<arr[i]+temp[i]<<" ";

    cout<<endl;

}
